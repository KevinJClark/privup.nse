NSE script to elevate privileges with the following:

```
$ sudo -l
user <username> may run the following commands on <hostname>:
    (ALL) NOPASSWD: /usr/bin/nmap
```